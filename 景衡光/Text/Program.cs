﻿using System;

namespace Text
{
    class Program
    {
        static void Main(string[] args)
        {

            // 打印九九乘法表

            for (int i = 1; i < 10; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write("{0}x{1}={2}\t",j,i,j*i);
                }
                Console.WriteLine("");
            }
           
        }
    }
}
