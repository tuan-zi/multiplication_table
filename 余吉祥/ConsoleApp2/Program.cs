﻿using System;
using System.Globalization;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            for(int i=1;i<=9;i++)
            {
                for(int j=1; j<=i;j++){
                    Console.Write(j + "x" + i + "=" + j * i+"\t");
                }
                Console.WriteLine();
            }
        }
    }
}
